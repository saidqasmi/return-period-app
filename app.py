import xarray as xr
import numpy as np
import pandas as pd
import os
import dash_leaflet as dl
from dash import Dash, html, Output, Input, dcc
import plotly.express as px
import plotly.graph_objects as go

basepath = "./data/"

stations_path = "assets/stations.json"

cluster = dl.GeoJSON(id="markers", url=f"/{stations_path}",
                     children=[dl.Tooltip(id="tooltip")])

bounds = [
        #south west
        [47, 0],
        #north east
        [48, 1.5]
        ]

variables = ['TX','TN']
years = list(range(1971, 2100))
rps = list(range(2, 101))

app = Dash(prevent_initial_callbacks=True)

app.layout = html.Div([

    html.Div(children=[
        html.Label('Select station'),
        html.Br(),
        dl.Map(center=[47.2, 2], minZoom=6, children=[dl.TileLayer(attribution=''), cluster], 
            style={'width': '75%', 'height': '60vh', 'margin': "auto", "display": "block"}
        ),
    ],style={'padding': 10, 'flex':0.5}),

    html.Div(children=[
        html.Label('Select variable'),
        dcc.Dropdown(variables, 'TX', id='list-variables'),
        html.Br(),
        html.Label('Select year'),
        dcc.Slider(years[0], years[-1], 1, value=2022, marks = None, tooltip={"placement": "bottom", "always_visible": True}, id='list-years'),
        #dcc.Dropdown(years, 2022, id='list-years'),
        dcc.Graph(id='plot-rl'),
        html.Br(),
        html.Label('Select return period'),
        dcc.Slider(rps[0], rps[-1], 1, value=50, marks = None, tooltip={"placement": "bottom", "always_visible": True}, id='list-rps'),
        #dcc.Dropdown(rps, 50, id='list-rps'),
        dcc.Graph(id='plot-ts'),
    ],style={'padding': 10, 'flex':0.8}),

],style={'display': 'flex', 'flex-direction': 'row'})

@app.callback(
    Output('plot-rl', 'figure'),
    Output('plot-ts', 'figure'),
    Input('list-years', 'value'),
    Input('list-rps', 'value'),
    Input("markers", "click_feature"))
def update_graph_rl(y, rp, feature):
    if feature is None:
        return None
    
    station = feature['properties']['insee']
    nom_usuel = feature['properties']['nom_usuel']

    df = xr.open_dataset( os.path.join( basepath , "%s_rl_quantiles.nc" % (station) ) )
    df_be = df.__xarray_dataarray_variable__.loc[0.5,y,:]#.values
    df_q05 = df.__xarray_dataarray_variable__.loc[0.05,y,:]#.values
    df_q95 = df.__xarray_dataarray_variable__.loc[0.95,y,:]#.values

    return_periods = df.__xarray_dataarray_variable__.indexes["return_period"]

    fig_rl = go.Figure()
    fig_rl.add_trace(go.Scatter(
        x=np.concatenate([return_periods, return_periods[::-1]]),
        y=xr.concat([df_q95, df_q05[::-1]],dim="return_period"),
        yhoverformat='.1f',
        fill='toself',
        line=dict(color="red"),
        hoveron='points',
        name="Confidence interval (95%)"
    ))

    fig_rl.add_trace(go.Scatter(
        x=return_periods,
        y=df_be,
        yhoverformat='.1f',
        mode='lines',
        line=dict(color="red"),
        hoveron='points',
        name="Best-estimate"
    ))

    fig_rl.add_vline(
        x=rp, line_width=3, line_dash="dot",
        annotation_text=str(rp)+" years", 
        annotation_position="bottom right",
        line_color="blue")

    fig_rl.update_layout(title="Return levels estimated in "+str(y)+" at station: "+str(station)+" | "+nom_usuel)
    fig_rl.update_xaxes(title_text='Return period (years)')
    fig_rl.update_yaxes(title_text='Return level (°C)')

    df_be = df.__xarray_dataarray_variable__.loc[0.5,:,rp]#.values
    df_q05 = df.__xarray_dataarray_variable__.loc[0.05,:,rp]#.values
    df_q95 = df.__xarray_dataarray_variable__.loc[0.95,:,rp]#.values

    years = df.__xarray_dataarray_variable__.indexes["time"]

    fig_ts = go.Figure()
    fig_ts.add_trace(go.Scatter(
        x=np.concatenate([years, years[::-1]]),
        y=xr.concat([df_q95, df_q05[::-1]],dim="time"),
        yhoverformat='.1f',
        fill='toself',
        hoveron='points',
        name="Confidence interval (95%)"
    ))

    fig_ts.add_trace(go.Scatter(
        x=years,
        y=df_be,
        yhoverformat='.1f',
        mode='lines',
        line=dict(color="blue"),
        hoveron='points',
        name="Best-estimate"
    ))

    fig_ts.add_vline(
        x=y, line_width=3, line_dash="dot",
        annotation_text=y, 
        annotation_position="bottom right",
        line_color="red")

    fig_ts.update_layout(title=str(rp)+"-yr return period at station: "+str(station)+" | "+nom_usuel)
    fig_ts.update_xaxes(title_text='Year')
    fig_ts.update_yaxes(title_text='Return level (°C)')

    return fig_rl, fig_ts



@app.callback(
        Output("tooltip", "children"),
        Input("markers", "hover_feature"))
def update_tooltip(feature):
    if feature is None:
        return None
    # Create a figure. Or an image. Or anything else :)
    fig = px.scatter(px.data.iris(), x="sepal_width", y="sepal_length")
    fig.layout.title = feature["properties"]["nom_usuel"]
    fig.layout.width = 300
    fig.layout.height = 200
    #return dcc.Graph(figure=fig)
    title = str(feature['properties']['insee'])+" | "+feature['properties']['nom_usuel']
    return title


#@app.callback(Output("state", "children"), [Input("states", "hover_feature")])
#def state_hover(feature):
#    if feature is not None:
#        return f"{feature['properties']['name']}"

if __name__ == '__main__':
    app.run_server(debug=True,host='127.0.0.1')



















 
